import { PostRequest } from '../../platform/web_ide';

export const getProjectCodeSuggestionsEnabled = (projectPath: string): PostRequest<void> => ({
  type: 'rest',
  method: 'POST',
  path: '/code_suggestions/enabled',
  body: { project_path: projectPath },
});
