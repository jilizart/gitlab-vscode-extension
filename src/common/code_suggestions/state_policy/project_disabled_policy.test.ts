import * as vscode from 'vscode';
import { GitLabPlatformForProject } from '../../platform/gitlab_platform';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import { ProjectDisabledPolicy } from './project_disabled_policy';
import { ResponseError } from '../../platform/web_ide';
import { GitLabPlatformManagerForCodeSuggestions } from '../gitlab_platform_manager_for_code_suggestions';
import { CS_DISABLED_PROJECT_CHECK_INTERVAL } from '../constants';

describe('ProjectDisabledPolicy', () => {
  let statusCode: number;
  let manager: GitLabPlatformManagerForCodeSuggestions;
  let policy: ProjectDisabledPolicy;
  let platform: GitLabPlatformForProject | undefined;
  const te = createFakePartial<vscode.TextEditor>({ document: { languageId: 'javascript' } });

  beforeEach(() => {
    platform = createFakePartial<GitLabPlatformForProject>({
      project: { namespaceWithPath: 'test' },
      fetchFromApi: jest.fn(() => {
        throw createFakePartial<ResponseError>({ status: statusCode });
      }),
    });

    statusCode = 403;
    manager = createFakePartial<GitLabPlatformManagerForCodeSuggestions>({
      getGitLabPlatform: async () => platform,
      onPlatformChange: jest.fn(),
    });
    policy = new ProjectDisabledPolicy(manager);
    vscode.window.activeTextEditor = te;
  });

  it('is not engaged when platform is missing', async () => {
    platform = undefined;

    await policy.init();

    expect(policy.engaged).toBe(false);
  });

  it('is not engaged when there is no active text editor', async () => {
    vscode.window.activeTextEditor = undefined;

    await policy.init();

    expect(policy.engaged).toBe(false);
  });

  it('is engaged if the API responded 403', async () => {
    await policy.init();

    expect(policy.engaged).toBe(true);
  });

  it('will cache the disabled status', async () => {
    await policy.init();

    statusCode = 400;

    await policy.init();

    // this would have been false without caching
    expect(policy.engaged).toBe(true);

    // API was not called the second time
    expect(platform?.fetchFromApi).toHaveBeenCalledTimes(1);
  });

  it('fires event when changed', async () => {
    const listener = jest.fn();
    policy.onEngagedChange(listener);

    await policy.init();

    expect(listener).toHaveBeenCalledWith(true);
  });

  it('listens on platform changes', async () => {
    jest.useFakeTimers();

    await policy.init();

    // any other status code than 403 results in disengaging this policy
    statusCode = 400;

    // make sure the cache won't give us old value
    jest.advanceTimersByTime(CS_DISABLED_PROJECT_CHECK_INTERVAL + 1);

    const listener = jest.fn();
    policy.onEngagedChange(listener);

    // simulate platform change
    await jest.mocked(manager.onPlatformChange).mock.calls[0][0](platform);

    expect(listener).toHaveBeenCalledWith(false);
  });

  it('listens on text editor changes', async () => {
    jest.useFakeTimers();

    await policy.init();

    // any other status code than 403 results in disengaging this policy
    statusCode = 400;

    // make sure the cache won't give us old value
    jest.advanceTimersByTime(CS_DISABLED_PROJECT_CHECK_INTERVAL + 1);

    const listener = jest.fn();
    policy.onEngagedChange(listener);

    // simulate text editor change change
    await jest.mocked(vscode.window.onDidChangeActiveTextEditor).mock.calls[0][0](te);

    expect(listener).toHaveBeenCalledWith(false);
  });
});
